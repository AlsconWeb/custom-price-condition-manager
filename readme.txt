=== Custom Price Condition Manager ===
Contributors: alexlavigin
Tags: wordpress woocommerce, woocommerce plugin, custom price manager, pricing conditions, pricing rules, discount management, special offers, e-commerce store, sales optimization, conversion rates, pricing strategy, discounts and offers, product pricing
Requires at least: 6.0
Tested up to: 6.3.1
Requires PHP: 7.4
Stable tag: 1.0.0
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Price Condition Manager is a versatile WordPress plugin designed to simplify the management of pricing conditions for your products.

== Description ==

Price Condition Manager is a versatile WordPress plugin designed to simplify the management of pricing
conditions for your products. With this plugin, you can easily create and apply various pricing rules, discounts,
and special offers to enhance your e-commerce store's pricing strategy.  Boost your sales and conversion rates while
optimizing your pricing strategy with Price Condition Manager.

**Important!** Custom Price Condition Manager plugin requires an active WooCommerce plugin.
